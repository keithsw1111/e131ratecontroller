#ifndef GLOBAL_H
	#define GLOBAL_H

	#include <sys/timeb.h>
	#include <list>
	#include <stdlib.h> 
	#include <string.h>
	#include "tinyxml2.h"
	#include <assert.h>
	using namespace std;

	#ifdef RASPBERRY
		#include <unistd.h>
		#include <pthread.h>
		#include <sys/socket.h>
		#include <netinet/in.h>
		#include <arpa/inet.h>	
		#define stricmp strcasecmp
		#include <stdint.h>
		#include <sys/time.h>
		#include <errno.h>

		#define _stricmp stricmp
		#define strcpy_s strcpy

	#else
		#include <WinSock2.h>
		#include <ws2tcpip.h>
		#include <Windows.h>
		#include <process.h>
	#endif

	#include <condition_variable>
	#include "spinner.h"
	#include "packet.h"
	#include "universe.h"
	#include "controller.h"
	
#endif