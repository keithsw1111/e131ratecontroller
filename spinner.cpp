#include "global.h"

Spinner::Spinner()
{
	_states[0] = '|';
	_states[1] = '/';
	_states[2] = '-';
	_states[3] = '\\'; 
	_current = 0;
}

char Spinner::Next()
{
	_current++;
	if (_current >= (int)sizeof(_states))
	{
		_current = 0;
	}
	
	//printf("Current %d, %c\n", _current, _states[_current]);
	return _states[_current];
}