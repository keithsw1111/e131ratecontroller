#ifndef CONTROLLER_H
	#define CONTROLLER_H

#ifdef RASPBERRY
void* ControllerRun(void* lpParam);
void* CopyRun(void* lpParam);
#else
DWORD WINAPI ControllerRun(LPVOID lpParam);
DWORD WINAPI CopyRun(LPVOID lpParam);
#endif

class Controller
{
	#ifdef RASPBERRY
		pthread_t _thread;
		pthread_t _copythread;
		int _socket;
		struct sockaddr_in _ipAddress;
		static int _socketcopyto;
		static struct sockaddr_in _ipAddresscopyto;
	#else
		HANDLE _thread;
		DWORD _threadId;
		HANDLE _copythread;
		DWORD _copythreadId;
		SOCKADDR_IN _ipAddress;
		SOCKET _socket;
		static SOCKADDR_IN _ipAddresscopyto;
		static SOCKET _socketcopyto;
	#endif

    std::mutex _outevent_mutex;
    std::condition_variable _outevent;
	int _id;
    int _port;
    int _ttl;
	int _inputpacketgapms;
    bool _filterunchangedpackets;
	bool _trimunchangedbytes;
	bool _minimalheader;
	long _minmillisbetweenpackets;
	int _maxburst;
	int _burstbytelimit;
	int _brightness;
	int _whiteadjustment;
	long _maxmillisbetweensends;
	list<Universe*>* _universelist;
	list<int> _universes;
	bool _debug;
	bool _silent;
	bool _uniqueportperoutput;
	Spinner _sendspinner;
	char _szipaddress[100];
	bool _trace;

public:
	~Controller();
	Controller(tinyxml2::XMLElement* n, list<Universe*>* universelist, bool debug, bool silent, bool uniqueportperoutput, int tracecontroller, int traceuniverse, int inputpacketgapms);
	void Run(); // check data and send it when necessary
	void CopyRunFunction();
	int Id() const
	{ return _id; };
	bool Trace() const
	{ return _trace; };
	static bool CopyToIP(char* ip, bool uniqueportperoutput, int port, int ttl);
};

#endif