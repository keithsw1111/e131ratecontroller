﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Xml;

namespace E131HealthMonitor
{
    class Universe
    {
        static Dictionary<int, string> __descriptions = new Dictionary<int, string>();
        static SortedDictionary<double, Color> __thresholds = new SortedDictionary<double, Color>();

        public int Number { get; set; } = -1;
        DateTime last = DateTime.MinValue;
        string _description = string.Empty;

        void LoadDescriptions()
        {
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.Load("E131HealthMonitor.xml");

                foreach(XmlNode n in doc.ChildNodes)
                {
                    if (n.Name.ToLower() == "e131healthmonitor")
                    {
                        foreach (XmlNode n1 in n.ChildNodes)
                        {
                            if (n1.Name.ToLower() == "universes")
                            {
                                foreach (XmlNode n2 in n1.ChildNodes)
                                {
                                    if (n2.Name.ToLower() == "universe")
                                    {
                                        int u = 0;
                                        string desc = "No Description";

                                        foreach (XmlNode n3 in n2.ChildNodes)
                                        {
                                            if (n3.Name.ToLower() == "number")
                                            {
                                                u = int.Parse(n3.InnerText);
                                            }
                                            else if (n3.Name.ToLower() == "description")
                                            {
                                                desc = n3.InnerText;
                                            }
                                        }

                                        __descriptions[u] = desc;
                                    }
                                }
                            }
                            else if (n1.Name.ToLower() == "thresholds")
                            {
                                foreach (XmlNode n2 in n1.ChildNodes)
                                {
                                    if (n2.Name.ToLower() == "threshold")
                                    {
                                        double threshold = -10;
                                        Color colour = Color.White;

                                        foreach (XmlNode n3 in n2.ChildNodes)
                                        {
                                            if (n3.Name.ToLower() == "value")
                                            {
                                                threshold = double.Parse(n3.InnerText);
                                            }
                                            else if (n3.Name.ToLower() == "colour")
                                            {
                                                colour = Color.FromName(n3.InnerText);
                                            }
                                        }

                                        __thresholds[threshold] =  colour;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                __descriptions[-1] = "Error loading descriptions " + ex.Message ;
            }
        }

        public Universe(int u)
        {
            Number = u;
            last = DateTime.Now;

            if (__descriptions.Count == 0)
            {
                LoadDescriptions();
            }
            if (__descriptions.ContainsKey(u))
            {
                _description = __descriptions[u];
            }
        }

        public override string ToString()
        {
            if (_description == string.Empty)
            {
                return Number.ToString("D3");
            }
            else
            {
                return Number.ToString("D3") + " - " + _description;
            }
        }

        public void Process()
        {
            last = DateTime.Now;
        }

        public Color Status
        {
            get
            {
                double diff = (DateTime.Now - last).TotalSeconds;
                List<double> dd = __thresholds.Keys.ToList();
                for (int i = dd.Count - 1; i >= 0; i--)
                {
                    if (diff > dd[i])
                    {
                        return __thresholds[dd[i]];
                    }
                }
                return Color.White;
            }
        }
    }
}
