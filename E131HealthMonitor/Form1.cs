﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;
using System.Net.Sockets;
using System.Threading;

namespace E131HealthMonitor
{
    public partial class Form1 : Form
    {
        Dictionary<int, Universe> _universes = new Dictionary<int, Universe>();
        Thread _t = null;
        bool _abort = false;
        bool _waiting = false;

        delegate void ProcessCallback(int u);

        private void Process(int u)
        {
            // InvokeRequired required compares the thread ID of the
            // calling thread to the thread ID of the creating thread.
            // If these threads are different, it returns true.

            if (this.InvokeRequired)
            {
                ProcessCallback d = new ProcessCallback(Process);
                try
                {
                    this.Invoke(d, new object[] { u });
                }
                catch
                {
                }
            }
            else
            {
                if (_universes.ContainsKey(u))
                {
                    _universes[u].Process();
                }
                else
                {
                    _universes[u] = new Universe(u);
                }
            }
        }

        class UdpState
        {
            public UdpClient u { get; set; }
            public IPEndPoint e { get; set; }
        };

        public void Run()
        {
            IPEndPoint ipep = new IPEndPoint(IPAddress.Any, 5568);
            UdpClient newsock = new UdpClient(ipep);
            IPEndPoint sender = new IPEndPoint(IPAddress.Any, 0);

            while (!_abort)
            {
                _waiting = true;
                UdpState s = new UdpState();
                s.e = sender;
                s.u = newsock;
                newsock.BeginReceive(ReceivePacket, s);
                while (_waiting & !_abort)
                {
                    Thread.Sleep(1);
                }
            }
        }

        void ReceivePacket(IAsyncResult result)
        {
            UdpClient u = (UdpClient)((UdpState)(result.AsyncState)).u;
            IPEndPoint sender = (IPEndPoint)((UdpState)(result.AsyncState)).e;

            byte[] message = new byte[4096];
            message = u.EndReceive(result, ref sender);
            DMXMessage msg = new DMXMessage(message);
            Process(msg.Universe);
            _waiting = false;
        }

        public Form1()
        {
            InitializeComponent();

            _t = new Thread(new ThreadStart(Run));
            _t.Start();
            timer1.Enabled = true;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            listView1.SuspendLayout();

            foreach(Universe u in _universes.Values)
            {
                bool found = false;
                foreach(ListViewItem lvi in listView1.Items)
                {
                    if (((Universe)(lvi.Tag)).Number == u.Number)
                    {
                        lvi.BackColor = u.Status;
                        found = true;
                        break;
                    }
                }
                if (!found)
                {
                    ListViewItem i = new ListViewItem(u.ToString());
                    i.Tag = u;
                    i.SubItems.Add(u.ToString());
                    listView1.Items.Add(i);
                }
            }
            listView1.ResumeLayout();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            _abort = true;
            //_t.Abort();
            //_t.Join();
        }
    }
}
