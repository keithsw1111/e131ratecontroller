echo "maximum socket connections"
sysctl net.core.somaxconn

echo ""
sysctl net.ipv4.udp

echo "min, pressure and max pages of memory for udp"
sysctl net.ipv4.udp_mem

echo "receive memory page size per socket allocation"
sysctl net.ipv4.udp_rmem_min

echo "send memory page size per socket allocation"
sysctl net.ipv4.udp_wmem_min

echo "maximum socket receive buffer"
sysctl net.core.rmem_max

echo "default socket receive buffer"
sysctl net.core.rmem_default

echo "maximum socket send buffer"
sysctl net.core.wmem_max

echo "defaule socket send buffer"
sysctl net.core.wmem_default

echo "Maximum number of packets in global input queue"
sysctl net.core.netdev_max_backlog

echo ""
sysctl net.core.optmem_max

echo "maximum open file descriptors"
ulimit -n
