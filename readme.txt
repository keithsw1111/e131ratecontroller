To build 

	make e131ratemanager

To change auto start:

   sudo nano /etc/rc.local

   add this line to the bottom
   
   exec /home/pi/e131ratemanager/e131ratemanager -q &

To kill it once running

   sudo killall e131ratemanager

To see if it is running

   ps a

Real time clock

	try setting up through FPP UI PiFace
	
	else add these to /etc/rc.local
	
	echo ds3231 0x68 > /sys/class/i2c-adapter/i2c-0/new_device
	hwclock -s
