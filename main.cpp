#ifdef RASPBERRY
#else
	#pragma comment(lib, "Ws2_32.lib")
#endif

#include "global.h"
#include "time.h"

int main(int argc, char *argv[])
{
	char configfile[E131DATASIZE];
	int traceuniverse = -1;
	int tracecontroller = -1;
	int buffermultiplier = 2;
	int inputpacketgapms = 50;
#ifdef RASPBERRY
		readlink("/proc/self/exe", configfile, sizeof(configfile));
	#else
		HMODULE hModule = GetModuleHandleW(nullptr);
		GetModuleFileNameA(hModule, configfile, sizeof(configfile));
	#endif
	printf("Runnning %s\n", configfile);
	int lastslash = -1;
	for(int i = strlen(configfile)-1; i >= 0; i--)
	{
		#ifdef RASPBERRY
			if (configfile[i] == '/')
		#else
			if (configfile[i] == '\\')
		#endif
		{
			lastslash = i;
			break;
		}
	}
	if (lastslash != -1)
	{
		strcpy(&configfile[lastslash+1], "E131RateManager.xml");
	}
	else
	{
		strcpy_s(configfile, "E131RateManager.xml");
	}
	printf("Config file %s\n", configfile);

	bool debug = false;
	bool silent = false;
	bool stats = false;
	for(int i = 1; i < argc; i++)
	{
		if (_stricmp(argv[i], "-d") == 0)
		{
			debug = true;
		}
		else if (_stricmp(argv[i], "-q") == 0)
		{
			silent = true;
		}
		else if (_stricmp(argv[i], "-s") == 0)
		{
			stats = true;
		}
	}

	if (debug)
	{
		printf("Debug statements ON.\n");
	}
	else
	{
		printf("Debug statements OFF. To turn them on run with the -d option.\n");
	}
	
	#ifdef RASPBERRY
	#else
	    WSADATA wsa;
		WSAStartup(MAKEWORD(2,2),&wsa);
	#endif
	
	int listenPort = 5568;
	list<Controller*> controllerlist;
	list<Universe*> universelist;

	tinyxml2::XMLDocument doc;
    doc.LoadFile(configfile);

	if (doc.FirstChildElement("E131RateManager") == nullptr)
	{
		printf("E131RateManager node not found in XML file.\n");
        return 2;
	}

	if (doc.FirstChildElement("E131RateManager")->FirstChildElement("Port") == nullptr)
	{
		printf("Port to listen to not found in XML file.\n");
	}

	if (doc.FirstChildElement("E131RateManager")->FirstChildElement("TraceUniverse") != nullptr && doc.FirstChildElement("E131RateManager")->FirstChildElement("TraceUniverse")->GetText() != nullptr)
	{
		const char* tu = doc.FirstChildElement("E131RateManager")->FirstChildElement("TraceUniverse")->GetText();
		traceuniverse = atoi(tu);
	}

	if (doc.FirstChildElement("E131RateManager")->FirstChildElement("TraceController") != nullptr && doc.FirstChildElement("E131RateManager")->FirstChildElement("TraceController")->GetText() != nullptr)
	{
		const char* tc = doc.FirstChildElement("E131RateManager")->FirstChildElement("TraceController")->GetText();
		tracecontroller = atoi(tc);
	}

	if (doc.FirstChildElement("E131RateManager")->FirstChildElement("BufferMultiplier") != nullptr && doc.FirstChildElement("E131RateManager")->FirstChildElement("BufferMultiplier")->GetText() != nullptr)
	{
		const char* bm = doc.FirstChildElement("E131RateManager")->FirstChildElement("BufferMultiplier")->GetText();
		buffermultiplier = atoi(bm);
		if (buffermultiplier < 2)
		{
			printf("**** BufferMultiplier less than 2 wont work ... value overridden.\n");
			buffermultiplier = 2;
		}
	}
	printf("BufferMultiplier=%d.\n", buffermultiplier);

	if (doc.FirstChildElement("E131RateManager")->FirstChildElement("InputPacketGapMS") != nullptr && doc.FirstChildElement("E131RateManager")->FirstChildElement("InputPacketGapMS")->GetText() != nullptr)
	{
		const char* ipg = doc.FirstChildElement("E131RateManager")->FirstChildElement("InputPacketGapMS")->GetText();
		inputpacketgapms = atoi(ipg);
	}
	printf("InputPacketGapMS=%d.\n", inputpacketgapms);

	const char* port = doc.FirstChildElement("E131RateManager")->FirstChildElement("Port")->GetText();
	listenPort = atoi(port);
	printf("Listen port %d\n", listenPort);

	if (doc.FirstChildElement("E131RateManager")->FirstChildElement("UniquePortPerOutput") == nullptr)
	{
		printf("UniquePortPerOutput not found in XML file.\n");
	}

	bool uniqueportperoutput = false;
	const char* cuniqueportperoutput = doc.FirstChildElement("E131RateManager")->FirstChildElement("UniquePortPerOutput")->GetText();
	if (_stricmp(cuniqueportperoutput, "true") == 0)
	{
		uniqueportperoutput = true;
	}
	printf("Unique port per output: %s\n", cuniqueportperoutput);

	// create the controllers & start them
	tinyxml2::XMLElement* controllers = doc.FirstChildElement("E131RateManager")->FirstChildElement("Controllers");
	tinyxml2::XMLElement* c = controllers->FirstChildElement("Controller");
	
	printf("Creating controllers.\n");
	while (c != nullptr)
	{
		controllerlist.push_back(new Controller(c, &universelist, debug, silent, uniqueportperoutput, tracecontroller, traceuniverse, inputpacketgapms));
		c = c->NextSiblingElement();
	}
	printf("Controllers created.\n\n");

	if (doc.FirstChildElement("E131RateManager")->FirstChildElement("CopyToIP") == nullptr || doc.FirstChildElement("E131RateManager")->FirstChildElement("CopyToIP")->GetText() == nullptr)
	{
		printf("Copy to IP address to not found in XML file.\n");
	}
	else
	{
		char szcopytoipaddress[100];
		strcpy_s(szcopytoipaddress, doc.FirstChildElement("E131RateManager")->FirstChildElement("CopyToIP")->GetText());
		if (strlen(szcopytoipaddress) > 0 && Universe::CopyToIP(szcopytoipaddress, uniqueportperoutput, listenPort, 1))
		{
			Controller::CopyToIP(szcopytoipaddress, uniqueportperoutput, listenPort, 1);
		}
		else
		{
			printf("Not copying to IP Address '%s'\n", szcopytoipaddress);
		}
	}

	if (tracecontroller >= 0)
	{
		printf("****** Trace Controller %d.\n", tracecontroller);
	}
	if (traceuniverse > 0)
	{
		printf("****** Trace Universe %d.\n", traceuniverse);
	}

	printf("Full list of universes being monitored: ");
	Universe::PrintUniverses(&universelist);
	printf("\n");

	// now listen for incoming packets
	#ifdef RASPBERRY
		int s = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
		if (s == -1)
		{
			printf("Error creating listening socket %d.\n", errno);
			return 1;
		}
		
		struct sockaddr_in addr;
		memset(&addr, 0x00, sizeof(addr));
		addr.sin_family = AF_INET;
		addr.sin_port = htons(listenPort);
		addr.sin_addr.s_addr = INADDR_ANY;

		if (bind(s, (struct sockaddr *) &addr, sizeof(addr)) != 0)
		{
			printf("Error binding to listening socket : %d.\n", errno);
			return 1;
		}

		//struct sockaddr_in remote_addr;
		//socklen_t iRemoteAddrLen = sizeof(remote_addr);

		int rcv;
		socklen_t size = sizeof(rcv);
		if (getsockopt(s, SOL_SOCKET, SO_RCVBUF, (char*)&rcv, &size) != -1)
		{
			printf("SO_RCVBUF:%d\n", rcv);
		}
		int snd;
		size = sizeof(snd);
		if (getsockopt(s, SOL_SOCKET, SO_SNDBUF, (char*)&snd, &size) != -1)
		{
			printf("SO_SNDBUF:%d\n", snd);
		}

		int universes = universelist.size();
		printf("Universes:%d\n", universes);
		rcv = buffermultiplier * universes * 1500;
		printf("Requested SO_RCVBUF:%d\n", rcv);
		size = sizeof(rcv);
		if (setsockopt(s, SOL_SOCKET, SO_RCVBUF, (char*)&rcv, size) != -1)
		{
			size = sizeof(rcv);
			if (getsockopt(s, SOL_SOCKET, SO_RCVBUF, (char*)&rcv, &size) != -1)
			{
				printf("New SO_RCVBUF:%d\n", rcv);
			}
		}
#else
		SOCKET s = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
		if (s == INVALID_SOCKET)
		{
			printf("Error creating listening socket %d.\n", WSAGetLastError());
		}

		SOCKADDR_IN addr;
		addr.sin_family = AF_INET;
		addr.sin_port = htons(listenPort);
		addr.sin_addr.s_addr = INADDR_ANY;

		if (::bind(s, reinterpret_cast<struct sockaddr *>(&addr), sizeof(addr)) != 0)
		{
			printf("Error binding listening socket : %d.\n", WSAGetLastError());
		}

		//SOCKADDR_IN remote_addr;
		//int iRemoteAddrLen = sizeof(remote_addr);

		unsigned int maxm;
		int size = sizeof(maxm);
		if (getsockopt(s, SOL_SOCKET, SO_MAX_MSG_SIZE, reinterpret_cast<char*>(&maxm), &size) != SOCKET_ERROR)
		{
			printf("SO_MAX_MSG_SIZE:%d\n", maxm);
		}
		int rcv;
		size = sizeof(rcv);
		if (getsockopt(s, SOL_SOCKET, SO_RCVBUF, reinterpret_cast<char*>(&rcv), &size) != SOCKET_ERROR)
		{
			printf("SO_RCVBUF:%d\n", rcv);
		}
		int snd;
		size = sizeof(snd);
		if (getsockopt(s, SOL_SOCKET, SO_SNDBUF, reinterpret_cast<char*>(&snd), &size) != SOCKET_ERROR)
		{
			printf("SO_SNDBUF:%d\n", snd);
		}

		int universes = universelist.size();
		printf("Universes:%d\n", universes);
		rcv = buffermultiplier * universes * 1500; // 2 packets of space per universe
		printf("Requested SO_RCVBUF:%d\n", rcv);
		size = sizeof(rcv);
		if (setsockopt(s, SOL_SOCKET, SO_RCVBUF, reinterpret_cast<char*>(&rcv), size) != SOCKET_ERROR)
		{
			size = sizeof(rcv);
			if (getsockopt(s, SOL_SOCKET, SO_RCVBUF, reinterpret_cast<char*>(&rcv), &size) != SOCKET_ERROR)
			{
				printf("New SO_RCVBUF:%d\n", rcv);
			}
		}
#endif

	unsigned char buffer[1500];
	int min = -1;

	printf("Starting to listen now - Press CTRL-C to stop.\n");
	while(true)
	{
		//iRemoteAddrLen = sizeof(remote_addr);
		//int packetlength = recvfrom(s, (char*)&buffer[0], sizeof(buffer), 0, (struct sockaddr *) &remote_addr, &iRemoteAddrLen);
		int packetlength = recv(s, reinterpret_cast<char*>(&buffer[0]), sizeof(buffer), 0);
		if (packetlength < 0)
		{
#ifdef RASPBERRY
			printf("Error receiving data : %d.\n", errno);
#else
			printf("Error receiving data : %d.\n", WSAGetLastError());
#endif
		}
		else
		{
			Packet p(buffer, packetlength, debug);

			if (p.Valid())
			{
				Universe *u = Universe::GetUniverse(&universelist, p.Universe());

				if (u == nullptr)
				{
					printf("Packet received from unexpected universe %d.\n", p.Universe());
				}
				else
				{
					u->Store(&p, debug);
				}
			}
			else
			{
				if (tracecontroller > 0 || traceuniverse > 0)
				{
					printf("Ignoring invalid packet of length %d.\n", packetlength);
				}
			}
		}

		time_t t = time(nullptr);
		struct tm* ptm = localtime(&t);
		if (stats && ptm->tm_min != min)
		{
			min = ptm->tm_min;

			printf("\n");
			Universe::PrintUniversesStats(&universelist);
			printf("\n");
		}
	}

	// return success
	return 0;
}
