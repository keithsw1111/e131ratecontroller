CC=g++
LD=ld
DEBUG= -g 
LDFLAGS= 
CFLAGS= -Wall -c -DRASPBERRY=1 -std=c++11 
TARGETS=e131ratemanager
LIBS=-pthread

all: $(TARGETS)

tinyxml2.o: tinyxml2.cpp tinyxml2.h
	$(CC) $(CFLAGS) tinyxml2.cpp

packet.o: packet.cpp packet.h global.h
	$(CC) $(CFLAGS) packet.cpp

e131ratemanager: main.o tinyxml2.o packet.o universe.o controller.o spinner.o
	$(CC) $(LDFLAGS) main.o spinner.o tinyxml2.o packet.o universe.o controller.o $(LIBS) -o e131ratemanager

universe.o: universe.cpp universe.h packet.h global.h
	$(CC) $(CFLAGS) universe.cpp

controller.o: controller.cpp controller.h universe.h packet.h spinner.h global.h
	$(CC) $(CFLAGS) controller.cpp

spinner.o: spinner.cpp spinner.h global.h
	$(CC) $(CFLAGS) spinner.cpp

main.o: main.cpp controller.h universe.h packet.h global.h
	$(CC) $(CFLAGS) main.cpp

clean:
	rm -rf $(TARGETS)
	rm -rf *.o
