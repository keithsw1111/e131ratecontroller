#include "global.h"
#include <time.h>

class Packet;

#ifdef RASPBERRY
int Universe::_socketcopyto = 0;
struct sockaddr_in Universe::_ipAddresscopyto;
#else
SOCKADDR_IN Universe::_ipAddresscopyto;
SOCKET Universe::_socketcopyto = 0;
#endif

Universe::~Universe()
{
}

void Universe::IncrementSequenceNumber()
{
	_sequenceNo++;
}

unsigned char* Universe::GetPacket(int* psize, bool minimalheader, bool trimtrailingsame, int brightness, int whiteadjustment)
{
	unsigned char* prc = nullptr;
	Packet* p = nullptr;

	{
        std::unique_lock<std::mutex> locker(_mutexout);

        if (_outPacket == nullptr)
		{
			printf("Odd ... _e131Packet should never be nullptr\n");
		}

		// create a copy of the packet
		p = new Packet(_outPacket);
		Sent();
	}

	if (p != nullptr)
	{
		IncrementSequenceNumber();
		p->SetSequenceNo(_sequenceNo);

		if (_lastE131Packet == nullptr)
		{
			p->SetTrailingSame(0);
		}
		else
		{
			p->SetTrailingSame(p->CountIdenticalTrailingBytes(_lastE131Packet));
		}

		*psize = PacketSize(p, minimalheader, trimtrailingsame);

		prc = static_cast<unsigned char *>(malloc(*psize));
		if (prc == nullptr)
		{
			printf("Error allocating memory for copy of packet.\n");
		}

		if (brightness == 255 && whiteadjustment == 255)
		{
			memcpy(prc, p->Buffer(minimalheader), *psize);
		}
		else
		{
			p->AdjustData(brightness, whiteadjustment);
			memcpy(prc, p->Buffer(minimalheader), *psize);
		}

		delete p;
	}

	return prc;
}

long Universe::getMilliCount()
{
	timeb tb;
	ftime(&tb);
	long nCount = tb.millitm + (tb.time & 0xfffff) * 1000;
	return nCount;
}

void Universe::Sent()
{
	if (_lastE131Packet != nullptr)
	{
		delete _lastE131Packet;
	}
	
    _lastE131Packet = new Packet(_outPacket);
	
	_lastSent = getMilliCount();
	_outCount++;
}

long Universe::MillisSinceSent() const
{
	long nSpan = getMilliCount() - _lastSent;
	if(nSpan < 0)
		nSpan += 0x100000 * 1000;
	return nSpan;
}

void Universe::Store(Packet* p, bool debug)
{
	bool trace = (_trace || _controller->Trace());

	{
        std::unique_lock<std::mutex> locker(_mutexin);
        _inCount++;
		if (trace)
		{
			time_t t = time(nullptr);
			printf("%ld Packet Controller %d Universe %d. Storing.\n", static_cast<long>(t), _controller->Id(), _universe);
		}

		if (debug)
		{
			printf("Storing packet for universe %d.\n", _universe);
		}
		if (_inPacket != nullptr)
		{
			delete _inPacket;
            _inPacket = nullptr;
		}
		_inPacket = new Packet(p);

        _inchanged = true;
		if (_inPacket == nullptr)
		{
			printf("Error storing packet.\n");
		}
    }
}

Universe::Universe(Controller* c, int u, char* ipaddress, int port, int ttl, bool uniqueportperoutput, std::condition_variable* outevent, std::mutex* outevent_mutex, int traceuniverse)
{
	if (u == traceuniverse)
	{
		_trace = true;
	}
	else
	{
		_trace = false;
	}
    _lastSent = 0;
	_inchanged = false;
	_controller = c;
	_outevent = outevent;
    _outevent_mutex = outevent_mutex;
	_uniqueportperoutput = uniqueportperoutput;
	_universe = u;
	_inPacket = nullptr;
	_outPacket = nullptr;
	_lastE131Packet = nullptr;
	int localport = 0;
	_inCount = 0;
	_outCount = 0;
	_sequenceNo = 0;
	strcpy_s(_szipaddress, ipaddress);
	#ifdef RASPBERRY
		if (_uniqueportperoutput)
		{
			_socket = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
			if (_socket == -1)
			{
				printf("Error creating socket %d.\n", errno);
			}
			memset(&_ipAddress, 0x00, sizeof(_ipAddress));
			_ipAddress.sin_family = AF_INET;
			_ipAddress.sin_port = htons(port);
			if (inet_aton(ipaddress, &_ipAddress.sin_addr) == 0)
			{
				printf("Error converting ip address %s.\n", ipaddress);
			}
			if (setsockopt(_socket, IPPROTO_IP, IP_TTL, (const char*)&ttl, sizeof(ttl)) != 0)
			{
				printf("Error setting socket TTL %d.\n", errno);
			}

			sockaddr_in local;
			local.sin_family = AF_INET;
			local.sin_addr.s_addr = INADDR_ANY;
			local.sin_port = 0; //randomly selected port
			if (bind(_socket, (sockaddr*)&local, sizeof(local)) != 0)
			{
				printf("Error binding to socket : %d.\n", errno);
			}

			struct sockaddr_in sin;
			socklen_t addrlen = sizeof(sin);
			if (getsockname(_socket, (struct sockaddr *)&sin, &addrlen) == 0 &&
				sin.sin_family == AF_INET &&
				addrlen == sizeof(sin))
			{
				localport = ntohs(sin.sin_port);
			}
			else
			{
				printf("Error getting socket local port : %d.\n", errno);
			}
		}
#else
		if (_uniqueportperoutput)
		{
			_socket = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
			if (_socket == INVALID_SOCKET)
			{
				printf("Error creating socket %d.\n", WSAGetLastError());
			}
			_ipAddress.sin_family = AF_INET;
			_ipAddress.sin_port = htons(port);
			if (inet_pton(AF_INET, ipaddress, &(_ipAddress.sin_addr)) != 1)
			{
				printf("Error converting ip address %s : %d.\n", ipaddress, WSAGetLastError());
			}
			int rc = setsockopt(_socket, IPPROTO_IP, IP_TTL, (const char*)&ttl, sizeof(ttl));
			if (rc != 0)
			{
				printf("Error setting socket TTL %d.\n", rc);
			}

			sockaddr_in local;
			local.sin_family = AF_INET;
			local.sin_addr.s_addr = INADDR_ANY;
			local.sin_port = 0; //randomly selected port
			if (::bind(_socket, reinterpret_cast<sockaddr*>(&local), sizeof(local)) != 0)
			{
				printf("Error binding to socket : %d.\n", WSAGetLastError());
			}

			struct sockaddr_in sin;
			int addrlen = sizeof(sin);
			if (getsockname(_socket, reinterpret_cast<struct sockaddr *>(&sin), &addrlen) == 0 &&
				sin.sin_family == AF_INET &&
				addrlen == sizeof(sin))
			{
				localport = ntohs(sin.sin_port);
			}
			else
			{
				printf("Error getting socket local port : %d.\n", WSAGetLastError());
			}
		}
#endif
	if (_uniqueportperoutput)
	{
		printf("                   IP Address: %d.%d.%d.%d Port %d\n", (_ipAddress.sin_addr.s_addr & 0x000000FF), ((_ipAddress.sin_addr.s_addr & 0x0000FF00) >> 8), ((_ipAddress.sin_addr.s_addr & 0x00FF0000) >> 16), ((_ipAddress.sin_addr.s_addr & 0xFF000000) >> 24), localport);
	}
}

bool Universe::IsReady()
{
    std::unique_lock<std::mutex> locker(_mutexout);
    return (_outPacket != nullptr);
}

bool Universe::CheckChange()
{
    std::unique_lock<std::mutex> locker(_mutexout);

    if (_outPacket == nullptr)
	{
		return false;
	}

	if (_lastE131Packet != nullptr)
	{
		return !_outPacket->IsSame(_lastE131Packet);
	}
	else
	{
		return true;
	}
}

void Universe::BufferData()
{
	Packet* p = nullptr;

    {
        std::unique_lock<std::mutex> locker(_mutexin);

        if (_inchanged && _inPacket != nullptr)
        {
            p = new Packet(_inPacket);
            _inchanged = false;
        }
    }

	if (p != nullptr)
	{
        {
            std::unique_lock<std::mutex> locker(_mutexout);
            if (_outPacket != nullptr)
            {
                delete _outPacket;
            }
            _outPacket = new Packet(p);
            delete p;
        }

        std::unique_lock<std::mutex> lk(*_outevent_mutex);
        lk.unlock();
        _outevent->notify_one();
	}
}

bool Universe::IsChanged()
{
	return CheckChange();
}

void Universe::CreateUniverse(Controller* c, list<Universe*>* universes, int u, char* ipaddress, int port, int ttl, bool uniqueportperoutput, std::condition_variable* event, std::mutex* event_mutex, int traceuniverse)
{
	// dont allow duplicates
	for (list<Universe*>::const_iterator ci = universes->begin(); ci != universes->end(); ++ci)
	{
		if ((*ci)->UniverseNo() == u)
		{
			return;
		}
	}

	Universe* pu = new Universe(c, u, ipaddress, port, ttl, uniqueportperoutput, event, event_mutex, traceuniverse);
	if (pu == nullptr)
	{
		printf("Error creating universe %d.\n", u);
	}
	universes->push_back(pu);
	printf("                       Universe %d created.\n", u);
}

Universe* Universe::GetUniverse(list<Universe*>* universes, int u)
{
	for (list<Universe*>::const_iterator ci = universes->begin(); ci != universes->end(); ++ci)
	{
		if ((*ci)->UniverseNo() == u)
		{
			return (*ci);
		}
	}

	printf("Attempt to get universe %d failed ... it does not exist.\n", u);

	return nullptr;
}

void Universe::PrintUniverses(list<Universe*>* universes)
{
	for (list<Universe*>::const_iterator ci = universes->begin(); ci != universes->end(); ++ci)
	{
		printf("%d ", (*ci)->UniverseNo());
	}
}

void Universe::PrintUniversesStats(list<Universe*>* universes)
{
	time_t t = time(nullptr);
	struct tm* pt = localtime(&t);
	printf("[%02d:%02d]\n", pt->tm_hour, pt->tm_min);
	int i = 0;
	for (list<Universe*>::const_iterator ci = universes->begin(); ci != universes->end(); ++ci)
	{
		printf("%03d: %ld -> %ld\t", (*ci)->UniverseNo(), (*ci)->InCount(), (*ci)->OutCount());
		i++;
		if (i % 3 == 0)
		{
			printf("\n");
		}
	}
}

int Universe::Send(bool minimalheader, bool trimtrailingsame, int brightness, int whiteadjustment)
{
	int size = 0;
	unsigned char* pdata = GetPacket(&size, minimalheader, trimtrailingsame, brightness, whiteadjustment);
	if (pdata != nullptr)
	{
		if (_trace)
		{
			time_t t = time(nullptr);
			printf("%ld Universe sending to universe %d.\n", static_cast<long>(t), _universe);
		}
		if (sendto(_socket, reinterpret_cast<const char *>(pdata), size, 0, reinterpret_cast<struct sockaddr *>(&_ipAddress), sizeof(_ipAddress))
#ifdef RASPBERRY
			== -1)
		{
			printf("Attempt to send packet failed : %d.\n", errno);
		}
#else
			== SOCKET_ERROR)
		{
			printf("Attempt to send packet failed : %d.\n", WSAGetLastError());
		}
#endif
		if (_socketcopyto != 0)
		{
			if (sendto(_socketcopyto, reinterpret_cast<const char *>(pdata), size, 0, reinterpret_cast<struct sockaddr *>(&_ipAddresscopyto), sizeof(_ipAddresscopyto))
#ifdef RASPBERRY
				== -1)
			{
				printf("Attempt to send packet to copy to failed : %d.\n", errno);
			}
#else
				== SOCKET_ERROR)
			{
				printf("Attempt to send packet to copy to failed : %d.\n", WSAGetLastError());
			}
#endif
		}

		free(pdata);
		pdata = nullptr;
	}

	return size;
}

bool Universe::CopyToIP(char* ip, bool uniqueportperoutput, int port, int ttl)
{
	int localport = 0;
#ifdef RASPBERRY
	if (uniqueportperoutput)
	{
		_socketcopyto = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
		if (_socketcopyto == -1)
		{
			printf("Error creating copy to socket %d.\n", errno);
		}
		memset(&_ipAddresscopyto, 0x00, sizeof(_ipAddresscopyto));
		_ipAddresscopyto.sin_family = AF_INET;
		_ipAddresscopyto.sin_port = htons(port);
		if (inet_aton(ip, &_ipAddresscopyto.sin_addr) == 0)
		{
			printf("Error converting copy to ip address %s.\n", ip);
			_socketcopyto = 0;
			return false;
		}
		if (setsockopt(_socketcopyto, IPPROTO_IP, IP_TTL, (const char*)&ttl, sizeof(ttl)) != 0)
		{
			printf("Error setting copy to socket TTL %d.\n", errno);
		}

		sockaddr_in local;
		local.sin_family = AF_INET;
		local.sin_addr.s_addr = INADDR_ANY;
		local.sin_port = 0; //randomly selected port
		if (bind(_socketcopyto, (sockaddr*)&local, sizeof(local)) != 0)
		{
			printf("Error binding to copy to socket : %d.\n", errno);
		}

		struct sockaddr_in sin;
		socklen_t addrlen = sizeof(sin);
		if (getsockname(_socketcopyto, (struct sockaddr *)&sin, &addrlen) == 0 &&
			sin.sin_family == AF_INET &&
			addrlen == sizeof(sin))
		{
			localport = ntohs(sin.sin_port);
		}
		else
		{
			printf("Error getting copy to socket local port : %d.\n", errno);
		}
	}
#else
	if (uniqueportperoutput)
	{
		_socketcopyto = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
		if (_socketcopyto == INVALID_SOCKET)
		{
			printf("Error creating socket to copy to %d.\n", WSAGetLastError());
		}
		_ipAddresscopyto.sin_family = AF_INET;
		_ipAddresscopyto.sin_port = htons(port);
		if (inet_pton(AF_INET, ip, &(_ipAddresscopyto.sin_addr)) != 1)
		{
			printf("Error converting copy to ip address %s : %d.\n", ip, WSAGetLastError());
			// an error occurred
			_socketcopyto = 0;
			return false;
		}
		int rc = setsockopt(_socketcopyto, IPPROTO_IP, IP_TTL, reinterpret_cast<const char*>(&ttl), sizeof(ttl));
		if (rc != 0)
		{
			printf("Error setting copy to socket TTL %d.\n", rc);
		}

		sockaddr_in local;
		local.sin_family = AF_INET;
		local.sin_addr.s_addr = INADDR_ANY;
		local.sin_port = 0; //randomly selected port
		if (::bind(_socketcopyto, reinterpret_cast<sockaddr*>(&local), sizeof(local)) != 0)
		{
			printf("Error binding to copy to socket : %d.\n", WSAGetLastError());
		}

		struct sockaddr_in sin;
		int addrlen = sizeof(sin);
		if (getsockname(_socketcopyto, reinterpret_cast<struct sockaddr *>(&sin), &addrlen) == 0 &&
			sin.sin_family == AF_INET &&
			addrlen == sizeof(sin))
		{
			localport = ntohs(sin.sin_port);
		}
		else
		{
			printf("Error getting copy to socket local port : %d.\n", WSAGetLastError());
		}
	}
#endif
	if (uniqueportperoutput)
	{
		printf("Copy to IP Address: %d.%d.%d.%d Port %d\n", (_ipAddresscopyto.sin_addr.s_addr & 0x000000FF), ((_ipAddresscopyto.sin_addr.s_addr & 0x0000FF00) >> 8), ((_ipAddresscopyto.sin_addr.s_addr & 0x00FF0000) >> 16), ((_ipAddresscopyto.sin_addr.s_addr & 0xFF000000) >> 24), localport);
	}

	return true;
}