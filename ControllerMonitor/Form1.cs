﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using System.Threading;

namespace E131HealthMonitor
{
    public partial class Form1 : Form
    {
        List<Controller> _controllers = new List<Controller>();
        bool _abort = false;
        Int32 _interval = 60;
        Thread _t = null;

        bool DuplicateController(Controller c)
        {
            foreach(Controller cc in _controllers)
            {
                if (cc.IPAddress.Equals(c.IPAddress))
                {
                    return true;
                }
            }
            return false;
        }

        public Form1()
        {
            InitializeComponent();

            XmlDocument doc = new XmlDocument();
            doc.Load("ControllerMonitor.xml");

            foreach (XmlNode n in doc.ChildNodes)
            {
                if (n.Name.ToLower() == "controllermonitor")
                {
                    foreach (XmlNode n1 in n.ChildNodes)
                    {
                        if (n1.Name.ToLower() == "controllers")
                        {
                            foreach (XmlNode n2 in n1.ChildNodes)
                            {
                                if (n2.Name.ToLower() == "controller")
                                {
                                    Controller c = new Controller(n2);
                                    _controllers.Add(c);
                                    ListViewItem i = new ListViewItem(c.ToString());
                                    i.Tag = c;
                                    i.SubItems.Add(c.ToString());
                                    listView1.Items.Add(i);
                                }
                            }
                        }
                        else if (n1.Name.ToLower() == "pinginterval")
                        {
                            // in seconds
                            _interval = Int32.Parse(n1.InnerText);
                        }
                    }
                }
            }

            doc = new XmlDocument();
            try
            {
                string x = Environment.CurrentDirectory;
                doc.Load("xlights_networks.xml");

                foreach (XmlNode n in doc.ChildNodes)
                {
                    if (n.Name.ToLower() == "networks")
                    {
                        foreach (XmlNode n1 in n.ChildNodes)
                        {
                            if (n1.Name.ToLower() == "network")
                            {
                                if ((n1.Attributes["NetworkType"].Value.ToLower() == "e131" ||
                                     n1.Attributes["NetworkType"].Value.ToLower() == "artnet" ||
                                     n1.Attributes["NetworkType"].Value.ToLower() == "ddp") &&
                                    (n1.Attributes["ComPort"].Value.ToLower() != "multicast"))
                                {
                                    Controller c = new Controller(n1);
                                    if (!DuplicateController(c))
                                    {
                                        _controllers.Add(c);
                                        ListViewItem i = new ListViewItem(c.ToString());
                                        i.Tag = c;
                                        i.SubItems.Add(c.ToString());
                                        listView1.Items.Add(i);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (FileNotFoundException fnf)
            {
                // thats ok
            }

            _t = new Thread(new ThreadStart(Run));
            _t.Start();
        }

        delegate void SetControllerColourCallback(Controller c, Color colour);

        private void SetControllerColour(Controller c, Color colour)
        {
            // InvokeRequired required compares the thread ID of the
            // calling thread to the thread ID of the creating thread.
            // If these threads are different, it returns true.
            if (this.listView1.InvokeRequired)
            {
                SetControllerColourCallback d = new SetControllerColourCallback(SetControllerColour);
                try
                {
                    this.Invoke(d, new object[] { c, colour });
                }
                catch
                {
                }
            }
            else
            {
                foreach (ListViewItem lvi in listView1.Items)
                {
                    if (lvi.Tag == c)
                    {
                        lvi.BackColor = colour;
                        break;
                    }
                }
            }
        }

        void UpdatePings()
        {
            foreach (Controller c in _controllers)
            {
                SetControllerColour(c, Color.LightBlue);
                c.Test();
                if (_abort)
                {
                    return;
                }
                SetControllerColour(c, c.Status);
            }
        }

        void Run()
        {
            while(!_abort)
            {
                DateTime start = DateTime.Now;

                UpdatePings();

                int left = _interval - (int)(DateTime.Now - start).TotalSeconds;

                if (left > 0)
                {
                    for (int i = 0; i < left; i++)
                    {
                        if (_abort)
                        {
                            break;
                        }
                        Thread.Sleep(1000);
                    }
                }
            }
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            _abort = true;
            //_t.Join();
        }

        private void listView1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if (listView1.SelectedItems.Count == 1)
            {
                Controller c = (Controller)listView1.SelectedItems[0].Tag;

                if (ModifierKeys.HasFlag(Keys.Control))
                {
                    System.Diagnostics.Process.Start("http://" + c.IPAddress.ToString());
                }
                else
                {
                    SetControllerColour(c, Color.LightBlue);
                    c.Test();
                    SetControllerColour(c, c.Status);
                    listView1.Items[listView1.SelectedIndices[0]].Selected = false;
                }
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
    }
}
