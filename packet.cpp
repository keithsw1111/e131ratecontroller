#include "global.h"
#include "packet.h"

// minimal packet (121 bytes smaller than a regular packet):
// 0xBC
// 1 byte sequence number
// 2 byte universe number
// packet data

Packet::Packet(Packet* p)
{
    _minimalpacket = nullptr;
    _packet = nullptr;
    
	_packet = static_cast<unsigned char *>(malloc(MAXPACKETSIZE));
	if (_packet == nullptr)
	{
		printf("Error allocating memory for packet.\n");
        return;
	}
	memset(_packet, 0x00, MAXPACKETSIZE);

    _minimalpacket = static_cast<unsigned char *>(malloc(MAXMINPACKETSIZE));
	if (_minimalpacket == nullptr)
	{
		printf("Error allocating memory for packet.\n");
        return;
	}
	memset(_minimalpacket, 0x00, MAXMINPACKETSIZE);
	memcpy(_packet, p->_packet, MAXPACKETSIZE);
	*_minimalpacket = 0xBC;
	*(_minimalpacket + 1) = *(p->_packet + 111);
	*(_minimalpacket + 2) = *(p->_packet + 113);
	*(_minimalpacket + 3) = *(p->_packet + 114);
	memcpy(_minimalpacket + MINHEADERSIZE, p->_packet + E131HEADERSIZE, E131DATASIZE);
	_size = p->_size;
	_minimalsize = p->_minimalsize;
	_trailingsame = p->_trailingsame;
    _debug = p->_debug;
}

void Packet::SetTrailingSame(int trailingsame)
{
	if (trailingsame < 0 || trailingsame > E131DATASIZE)
	{
		printf("Odd ... trailing same invalid %d.\n", trailingsame);
	}

	_trailingsame = trailingsame;

	// need to adjust size fields in the packet
}

Packet::~Packet()
{
    if (_packet != nullptr)
	{
		free(_packet);
        _packet = nullptr;
	}
    if (_minimalpacket != nullptr)
    {
        free(_minimalpacket);
        _minimalpacket = nullptr;
    }
}

Packet::Packet(unsigned char * p, int size, bool debug)
{
    _debug = debug;
    _minimalpacket = nullptr;
    _packet = nullptr;

	if (size < 0 || size > 638)
	{
		printf("Invalid packet size: %d.\n", size);
        return;
	}

	_size = size;
	_minimalsize = size - E131HEADERSIZE;
	_trailingsame = 0;
	_packet = static_cast<unsigned char *>(malloc(MAXPACKETSIZE));
	if (_packet == nullptr)
	{
		printf("Error allocating memory for packet.\n");
        return;
	}
	memset(_packet, 0x00, MAXPACKETSIZE);
	
    _minimalpacket = static_cast<unsigned char *>(malloc(MAXMINPACKETSIZE));
	if (_minimalpacket == nullptr)
	{
		printf("Error allocating memory for packet.\n");
        return;
	}
	memset(_minimalpacket, 0x00, MAXMINPACKETSIZE);
	memcpy(_packet, p, _size);
	*_minimalpacket = 0xBC;
	*(_minimalpacket+1) = *(p + 111);
	*(_minimalpacket+2) = *(p + 113);
	*(_minimalpacket+3) = *(p + 114);
	memcpy(_minimalpacket+ MINHEADERSIZE, p + E131HEADERSIZE, _minimalsize);
}

int Packet::Universe() const
{
	return (static_cast<int>(_packet[113]) << 8) + static_cast<int>(_packet[114]);
}

uint8_t Packet::SequenceNo() const
{
	return _packet[111];
}

// Compare the data area of two packets to see if they are identical
bool Packet::IsSame(Packet* p) const
{
	return (memcmp(&_packet[E131HEADERSIZE], &p->_packet[E131HEADERSIZE], _size - E131HEADERSIZE) == 0);
}

// apply a brightness adjustment
void Packet::AdjustData(int brightness, int whiteadjustment)
{
	for (int i = E131HEADERSIZE; i < _size; i++)
	{
		unsigned char b = 0;
		if (brightness > 0)
		{
			b = (unsigned char)((int)_packet[i] * brightness / 255);
		}

		_packet[i] = b;
		_minimalpacket[i - 121] = b;
	}

	if (brightness == 255 && whiteadjustment != 255)
	{
        if ((_size - E131HEADERSIZE) % 3 != 0)
        {
            if (_debug)
            {
                printf("Packet for Universe %d was %d long which is not divisible by 3 ... white adjustment skipped.\n", this->Universe(), _size - E131HEADERSIZE);
            }
        }
        else
        {
            for (int i = E131HEADERSIZE; i < _size - 2; i += 3)
            {
                if (_packet[i] == _packet[i + 1] == 255 && _packet[i] == _packet[i + 2])
                {
                    unsigned char newbright = (unsigned char)((int)_packet[i] * whiteadjustment / 255);
                    _packet[i] = newbright;
                    _packet[i + 1] = newbright;
                    _packet[i + 2] = newbright;
                    _minimalpacket[i - 121] = newbright;
                    _minimalpacket[i - 121 + 1] = newbright;
                    _minimalpacket[i - 121 + 2] = newbright;
                }
            }
        }
	}
}

// Count the number of bytes in the packet from the end of the data that are the same as the last packet
int Packet::CountIdenticalTrailingBytes(Packet* p)
{
	if (_size != p->_size)
	{
		return 0;
	}

	for (int i = _size -1 ; i >= E131HEADERSIZE; i--)
	{
		if (_packet[i] != p->_packet[i])
		{
			int size = _size - i + 1;

			// force it to be a multiple of 3 so i dont cut off part of a pixel
			size -= (size % 3);

			return size;
		}
	}
	return 0;
}

int Packet::Size(bool minimalheader, bool trimtrailingsame)
{
	if (minimalheader)
	{
		if (trimtrailingsame)
		{
			return _size - E131HEADERSIZE - _trailingsame;
		}
		else
		{
			return _size - E131HEADERSIZE;
		}
	}
	else
	{
		if (trimtrailingsame && _trailingsame > 0)
		{
			return _size - _trailingsame;
		}
		else
		{
			return _size;
		}
	}
}

unsigned char * Packet::Buffer(bool minimalheader) const
{
	if (minimalheader)
	{
		return _minimalpacket;
	}
	else
	{
		return _packet;
	}
}

bool Packet::Valid() const
{
	if (_size < E131HEADERSIZE)
	{
		return false;
	}

    if (_size > E131HEADERSIZE + E131DATASIZE)
    {
        return false;
    }

	unsigned char valid[] = {0x41, 0x53, 0x43, 0x2d, 0x45, 0x31, 0x2e, 0x31, 0x37, 0x00, 0x00, 0x00 };

	if (memcmp(&_packet[MINHEADERSIZE], valid, sizeof(valid)) != 0)
	{
		printf("Packet invalid.\n");
		return false;
	}

	return true;
}

void Packet::SetSequenceNo(uint8_t seq)
{
	*(_packet + 111) = seq;
	*(_minimalpacket + 1) = seq;
}
