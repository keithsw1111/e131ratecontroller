﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace E131HealthMonitor
{
    class DMXMessage
    {
        byte[] _buffer;
        public int Universe { get; protected set; }

        void CheckByte(short offset, byte b)
        {
            if (_buffer[offset] != b)
            {
                throw new ApplicationException("DMX Packet validation failed.");
            }
        }

        void CheckWord(short offset, UInt16 w)
        {
            if ((_buffer[offset] != (w & 0xFF00) >> 8) ||
                (_buffer[offset + 1] != (w & 0x00FF)))
            {
                throw new ApplicationException("DMX Packet validation failed.");
            }
        }

        void CheckLong(short offset, UInt32 ul)
        {
            if ((_buffer[offset] != (ul & 0xFF000000) >> 24) ||
                (_buffer[offset + 1] != (ul & 0x00FF0000) >> 16) ||
                (_buffer[offset + 2] != (ul & 0x0000FF00) >> 8) ||
                (_buffer[offset + 3] != (ul & 0x000000FF)))
            {
                throw new ApplicationException("DMX Packet validation failed.");
            }
        }

        void ValidatePacket()
        {
            CheckWord(0, 0x0010);
            CheckWord(2, 0x0000);
            CheckLong(4, 0x4153432D);
            CheckLong(8, 0x45312E31);
        }

        public DMXMessage(byte[] buffer)
        {
            Universe = -1;
            _buffer = buffer;

            if (_buffer[0] == 0xBC)
            {
                // minimal header packet
                Universe = (_buffer[2] << 8) + _buffer[3];
            }
            else
            {
                // normal packet
                try
                {
                    ValidatePacket();
                    Universe = (_buffer[113] << 8) + _buffer[114];
                }
                catch (Exception ex)
                {
                    // invalid packet
                    Universe = -1;
                }
            }
        }
    }
}
