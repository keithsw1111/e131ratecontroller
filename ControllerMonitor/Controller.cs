﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Xml;
using System.Net;
using System.Net.NetworkInformation;

namespace E131HealthMonitor
{
    class Controller
    {
        IPAddress _ip = IPAddress.None;
        string _name = string.Empty;
        bool _result = false;

        public IPAddress IPAddress
        {
            get { return _ip; }
        }

        public void Test()
        {
            Ping p = new Ping();
            PingReply r = p.Send(_ip);

            _result = (r.Status == IPStatus.Success);
        }

        public Controller(XmlNode n)
        {
            if (n.Attributes["ComPort"] != null)
            {
                if (n.Attributes["Description"] != null)
                {
                    _name = n.Attributes["Description"].Value;
                }
                else
                {
                    _name = n.Attributes["ComPort"].Value;
                }
                _ip = IPAddress.Parse(n.Attributes["ComPort"].Value);
            }
            else
            {
                foreach (XmlNode n1 in n.ChildNodes)
                {
                    if (n1.Name.ToLower() == "name")
                    {
                        _name = n1.InnerText;
                    }
                    else if (n1.Name.ToLower() == "ip")
                    {
                        _ip = IPAddress.Parse(n1.InnerText);
                    }
                }
            }

            _result = false;
        }

        public override string ToString()
        {
            return _name + " : " + _ip.ToString();
        }

        public Color Status
        {
            get
            {
                if (_result)
                {
                    return Color.Green;
                }
                else
                {
                    System.Media.SystemSounds.Asterisk.Play();
                    return Color.Red;
                }
            }
        }
    }
}
