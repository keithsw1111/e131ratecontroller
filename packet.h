#ifndef PACKET_H
	#define PACKET_H

    #define E131HEADERSIZE 126
    #define MINHEADERSIZE 4
    #define E131DATASIZE 512
    #define MAXPACKETSIZE E131HEADERSIZE+E131DATASIZE
	#define MAXMINPACKETSIZE MINHEADERSIZE+E131DATASIZE

	class Packet
	{
		unsigned char* _packet;
		unsigned char* _minimalpacket;
		int _size;
		int _minimalsize;
		int _trailingsame;
        bool _debug;

	public:
		Packet(unsigned char * p, int size, bool debug);
		Packet(Packet* p);
		void SetTrailingSame(int trailingsame);
		~Packet();
		int Size(bool minimalheader, bool trimtrailingsame);
		int Universe() const;
		uint8_t SequenceNo() const;
		bool Valid() const;
		bool IsSame(Packet* p) const;
		int CountIdenticalTrailingBytes(Packet* p);
		unsigned char * Buffer(bool minimalheader) const;
		void AdjustData(int brightness, int whiteadjustment);
		void SetSequenceNo(uint8_t seq);
	};

#endif