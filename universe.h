#ifndef UNIVERSE_H
	#define UNIVERSE_H

	#include <mutex>

	class Controller;

	class Universe
	{
        std::mutex _mutexin;
        std::mutex _mutexout;
        #ifdef RASPBERRY
		    int _socket;
			struct sockaddr_in _ipAddress;
			static int _socketcopyto;
			static struct sockaddr_in _ipAddresscopyto;
		#else
    		SOCKADDR_IN _ipAddress;
			SOCKET _socket;
			static SOCKADDR_IN _ipAddresscopyto;
			static SOCKET _socketcopyto;
		#endif
		std::condition_variable* _outevent;
        std::mutex* _outevent_mutex;
		Controller* _controller;
		int _universe;
		Packet* _lastE131Packet;
		Packet* _outPacket;
		Packet* _inPacket;
		long _lastSent;
	    static long getMilliCount();
		bool _uniqueportperoutput;
		bool _inchanged;
		char _szipaddress[100];

	    static int PacketSize(Packet* p, bool minimalheader, bool trimtrailingsame) { return p->Size(minimalheader, trimtrailingsame); };
		bool _trace;
		bool CheckChange();
		void Sent(); // called when the packet has been sent
		long _inCount;
		long _outCount;
		uint8_t _sequenceNo;
		void IncrementSequenceNumber();

	public:
		~Universe();
		Universe(Controller* c, int u, char* ipaddress, int port, int ttl, bool uniqueportperoutput, std::condition_variable* event, std::mutex* event_mutex, int traceuniverse);
		void Store(Packet* p, bool debug);
		bool IsChanged(); // check if it has changed
		unsigned char* GetPacket(int* psize, bool minimalheader, bool trimtrailingsame, int brightness, int whiteadjustment);
		int Send(bool minimalheader, bool trimtrailingsame, int brightness, int whiteadjustment);
		long MillisSinceSent() const;
		int UniverseNo() const
		{ return _universe; };
		bool Trace() const
		{ return _trace; };
		bool IsReady();
		long InCount() const
		{ return _inCount; };
		long OutCount() const
		{ return _outCount; };
		static Universe* GetUniverse(list<Universe*>* universes, int u);
		static void PrintUniverses(list<Universe*>* universes);
		static void PrintUniversesStats(list<Universe*>* universes);
		static void CreateUniverse(Controller* c, list<Universe*>* universes, int u, char* ipaddress, int port, int ttl, bool uniqueportperoutput, std::condition_variable* event, std::mutex* event_mutex, int traceuniverse);
		static bool CopyToIP(char* ip, bool uniqueportperoutput, int port, int ttl);
		void BufferData();
	};

#endif