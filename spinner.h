#ifndef SPINNER_H
	#define SPINNER_H

class Spinner
{
	int _current;
	char _states[4];

public:
	Spinner();
	char Next();
};

#endif