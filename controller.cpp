// this is effectively done
// The controller runs on its own thread checking universe data peridically and sending it on to the controllers.

#include "global.h"
#include <time.h>

#ifdef RASPBERRY
#define _stricmp stricmp
#define strcpy_s strcpy
#endif

#ifdef RASPBERRY
int Controller::_socketcopyto = 0;
struct sockaddr_in Controller::_ipAddresscopyto;
#else
SOCKADDR_IN Controller::_ipAddresscopyto;
SOCKET Controller::_socketcopyto = 0;
#endif

bool Controller::CopyToIP(char* ip, bool uniqueportperoutput, int port, int ttl)
{
	int localport = 0;
#ifdef RASPBERRY
	if (!uniqueportperoutput)
	{
		_socketcopyto = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
		if (_socketcopyto == -1)
		{
			printf("Error creating copy to socket %d.\n", errno);
		}
		memset(&_ipAddresscopyto, 0x00, sizeof(_ipAddresscopyto));
		_ipAddresscopyto.sin_family = AF_INET;
		_ipAddresscopyto.sin_port = htons(port);
		if (inet_aton(ip, &_ipAddresscopyto.sin_addr) == 0)
		{
			printf("Error converting copy to ip address %s.\n", ip);
			_socketcopyto = 0;
			return false;
		}
		if (setsockopt(_socketcopyto, IPPROTO_IP, IP_TTL, (const char*)&ttl, sizeof(ttl)) != 0)
		{
			printf("Error setting copy to socket TTL %d.\n", errno);
		}

		sockaddr_in local;
		local.sin_family = AF_INET;
		local.sin_addr.s_addr = INADDR_ANY;
		local.sin_port = 0; //randomly selected port
		if (bind(_socketcopyto, (sockaddr*)&local, sizeof(local)) != 0)
		{
			printf("Error binding to copy to socket : %d.\n", errno);
		}

		struct sockaddr_in sin;
		socklen_t addrlen = sizeof(sin);
		if (getsockname(_socketcopyto, (struct sockaddr *)&sin, &addrlen) == 0 &&
			sin.sin_family == AF_INET &&
			addrlen == sizeof(sin))
		{
			localport = ntohs(sin.sin_port);
		}
		else
		{
			printf("Error getting copy to socket local port : %d.\n", errno);
		}
	}
#else
	if (!uniqueportperoutput)
	{
		_socketcopyto = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
		if (_socketcopyto == INVALID_SOCKET)
		{
			printf("Error creating socket to copy to %d.\n", WSAGetLastError());
		}
		_ipAddresscopyto.sin_family = AF_INET;
		_ipAddresscopyto.sin_port = htons(port);
		if (inet_pton(AF_INET, ip, &(_ipAddresscopyto.sin_addr)) != 1)
		{
			printf("Error converting copy to ip address %s : %d.\n", ip, WSAGetLastError());
			_socketcopyto = 0;
			return false;
		}
		int rc = setsockopt(_socketcopyto, IPPROTO_IP, IP_TTL, reinterpret_cast<const char*>(&ttl), sizeof(ttl));
		if (rc != 0)
		{
			printf("Error setting copy to socket TTL %d.\n", rc);
		}

		sockaddr_in local;
		local.sin_family = AF_INET;
		local.sin_addr.s_addr = INADDR_ANY;
		local.sin_port = 0; //randomly selected port
		if (::bind(_socketcopyto, reinterpret_cast<sockaddr*>(&local), sizeof(local)) != 0)
		{
			printf("Error binding to copy to socket : %d.\n", WSAGetLastError());
		}

		struct sockaddr_in sin;
		int addrlen = sizeof(sin);
		if (getsockname(_socketcopyto, reinterpret_cast<struct sockaddr *>(&sin), &addrlen) == 0 &&
			sin.sin_family == AF_INET &&
			addrlen == sizeof(sin))
		{
			localport = ntohs(sin.sin_port);
		}
		else
		{
			printf("Error getting copy to socket local port : %d.\n", WSAGetLastError());
		}
	}
#endif
	if (!uniqueportperoutput)
	{
		printf("Copy to IP Address: %d.%d.%d.%d Port %d.\n", (_ipAddresscopyto.sin_addr.s_addr & 0x000000FF), ((_ipAddresscopyto.sin_addr.s_addr & 0x0000FF00) >> 8), ((_ipAddresscopyto.sin_addr.s_addr & 0x00FF0000) >> 16), ((_ipAddresscopyto.sin_addr.s_addr & 0xFF000000) >> 24), localport);
	}
	return true;
}

void Controller::CopyRunFunction()
{
	while (true)
	{
		for (list<Universe*>::const_iterator ci = _universelist->begin(); ci != _universelist->end(); ++ci)
		{
			Universe* u = *ci;
			u->BufferData();
		}
#ifdef RASPBERRY
		usleep(_inputpacketgapms * 1000);
#else
		Sleep(_inputpacketgapms);
#endif
	}
}

// this will not work with just one universe
void Controller::Run()
{
	printf("Controller %d sending thread created.\n", _id);
	list<int>::const_iterator i = _universes.begin();

    while (true)
    {
        int burstcount = 0;
        long burstbytecount = 0;
        list<int>::const_iterator start = i;

        // send a burst
        if (_debug)
        {
            printf("Sending a burst of data.\n");
        }

        do
        {
            Universe* pUniverse = Universe::GetUniverse(_universelist, *i);
            if (pUniverse == nullptr)
            {
                printf("Odd ... pUniverse should never be nullptr\n");
            }

            if (_debug)
            {
                printf("Start send loop for universe %d.\n", pUniverse->UniverseNo());
            }

            // send packets
            bool foverdue = pUniverse->MillisSinceSent() > _maxmillisbetweensends && _maxmillisbetweensends >= 0;
            if ((!_filterunchangedpackets || pUniverse->IsChanged() || foverdue) && pUniverse->IsReady())
            {
                if (!foverdue)
                {
                }
                else
                {
                    if (_trace || pUniverse->Trace())
                    {
                        printf("Controller %d universe %d overdue to receive a packet.\n", _id, pUniverse->UniverseNo());
                    }
                }
                if (_debug)
                {
                    printf("Controller %d passing on packet to universe %d.\n", _id, pUniverse->UniverseNo());
                }
                int size = 0;
                if (_uniqueportperoutput)
                {
                    size = pUniverse->Send(_minimalheader, _trimunchangedbytes, _brightness, _whiteadjustment);
                }
                else
                {
                    unsigned char* pdata = pUniverse->GetPacket(&size, _minimalheader, _trimunchangedbytes, _brightness, _whiteadjustment);
                    if (pdata != nullptr)
                    {
                        if (_trace)
                        {
                            time_t t = time(nullptr);
                            printf("%ld Controller %d Sending to universe %d.\n", static_cast<long>(t), _id, pUniverse->UniverseNo());
                        }
                        if (sendto(_socket, (const char *)pdata, size, 0, reinterpret_cast<struct sockaddr *>(&_ipAddress), sizeof(_ipAddress))
#ifdef RASPBERRY
                            == -1)
                        {
                            printf("Attempt to send packet failed : %d.\n", errno);
                        }
#else
                            == SOCKET_ERROR)
                        {
                            printf("Attempt to send packet failed : %d.\n", WSAGetLastError());
                        }
#endif
                        if (_socketcopyto != 0)
                        {
                            if (sendto(_socketcopyto, reinterpret_cast<const char *>(pdata), size, 0, reinterpret_cast<struct sockaddr *>(&_ipAddresscopyto), sizeof(_ipAddresscopyto))
#ifdef RASPBERRY
                                == -1)
                            {
                                printf("Attempt to send packet to copy to ip failed : %d.\n", errno);
                            }
#else
                                == SOCKET_ERROR)
                            {
                                printf("Attempt to send packet to copy to ip failed : %d.\n", WSAGetLastError());
                            }
#endif
                        }
                        free(pdata);
                        pdata = nullptr;
                    }
                }
                burstcount++;
                burstbytecount += size;
            }
            else
            {
                if (_debug)
                {
                    printf("No data to send for for universe %d.\n", pUniverse->UniverseNo());
                }
            }

            if (_debug)
            {
                printf("End send loop for universe %d.\n", pUniverse->UniverseNo());
            }

            i++;
            if (i == _universes.end())
            {
                i = _universes.begin();
            }
        } while (start != i && (_maxburst == -1 || burstcount < _maxburst) && (_burstbytelimit == -1 || burstbytecount < _burstbytelimit));

        if (_debug)
        {
            printf("End of sending a burst of data.\n");
            printf("Start inter burst.\n");
        }

        // if we stopped due to too much to do
        if (burstcount > 0) // && !onlysentduetomillissincesent)
        {
            // sleep
            if (_debug && _minmillisbetweenpackets >= 0)
            {
                printf("Controller %d sleeping for %ldms after sending %d packets.\n", _id, _minmillisbetweenpackets, burstcount);
            }
            else
            {
                if (!_silent)
                {
                    printf(" %c\n\x1b[1A", _sendspinner.Next());
                    fflush(stdout);
                }
            }

            if (_minmillisbetweenpackets > 0)
            {
#ifdef RASPBERRY
                usleep(_minmillisbetweenpackets * 1000);
#else
                Sleep(_minmillisbetweenpackets);
#endif
            }
            else
            {
            }
        }
        else
        {
            // now can wait until one of my packets change or timeout
            clock_t starttm = clock();
            if (_debug)
            {
                printf("%ld %s waiting.\n", reinterpret_cast<long>(_outevent.native_handle()), _szipaddress);
            }
            std::unique_lock<std::mutex> lk(_outevent_mutex);
            if (_maxmillisbetweensends <= 0)
            {
                _outevent.wait(lk);
            }
            else
            {
                _outevent.wait_for(lk, std::chrono::milliseconds(_maxmillisbetweensends));
            }
            if (_debug)
            {
                printf("%ld %s slept for %ld waiting for a packet.\n", reinterpret_cast<long>(_outevent.native_handle()), _szipaddress, (long)(clock() - starttm));
            }
        }

        if (_debug)
        {
            printf("End inter burst.\n");
        }
    }

	printf("Controller %d sending thread exiting.\n", _id);
}

Controller::~Controller()
{
}

Controller::Controller(tinyxml2::XMLElement* n, list<Universe*>* universelist, bool debug, bool silent, bool uniqueportperouput, int tracecontroller, int traceuniverse, int inputpacketgapms)
{
	_inputpacketgapms = inputpacketgapms;
	_trace = false;
	_debug = debug;
	_silent = silent;
	_uniqueportperoutput = uniqueportperouput;
	_universelist = universelist;
	_port = 5568;
	_ttl = 1;
	_filterunchangedpackets = true;
	_trimunchangedbytes = false;
	_minimalheader = false;
	_minmillisbetweenpackets = -1; // -1 = disabled
	_maxburst = 1; // -1 = disabled
	_burstbytelimit = -1; // -1 = disabled
	_maxmillisbetweensends = -1; // -1 = disabled
	_brightness = 255;
	_whiteadjustment = 255;
	strcpy_s(_szipaddress, "");

	if (n->FirstChildElement("Id") == nullptr)
	{
		printf("Error reading controller id.\n");
	}
	else
	{
		_id = atoi(n->FirstChildElement("Id")->GetText());
		printf("               Controller created Id: %d\n", _id);
	}

	if (_id == tracecontroller)
	{
		_trace = true;
	}

	if (n->FirstChildElement("Port") == nullptr)
	{
		printf("Error reading port for controller %d.\n", _id);
	}
	else
	{
		_port = atoi(n->FirstChildElement("Port")->GetText());
		printf("                   Send to port: %d\n", _port);
	}

	if (n->FirstChildElement("FilterUnchangedPackets") == nullptr)
	{
		printf("Error reading Filter Unchanged Packets setting for controller %d.\n", _id);
	}
	else
	{
		if (_stricmp(n->FirstChildElement("FilterUnchangedPackets")->GetText(), "false") == 0)
		{
			_filterunchangedpackets = false;
		}
		printf("                   Filter unchanged packets: %d\n", _filterunchangedpackets);
	}

	if (n->FirstChildElement("TrimUnchangedTrailingData") == nullptr)
	{
		printf("Error reading Trim Unchanged Bytes setting for controller %d.\n", _id);
	}
	else
	{
		if (_stricmp(n->FirstChildElement("TrimUnchangedTrailingData")->GetText(), "true") == 0)
		{
			_trimunchangedbytes = true;
		}
		printf("                   Trim unchanged bytes: %d\n", _trimunchangedbytes);
	}

	if (n->FirstChildElement("MinimalHeader") == nullptr)
	{
		printf("Error reading Minimal Header setting for controller %d.\n", _id);
	}
	else
	{
		if (_stricmp(n->FirstChildElement("MinimalHeader")->GetText(), "true") == 0)
		{
			_minimalheader = true;
		}
		printf("                   Minimal header: %d\n", _minimalheader);
	}

	if (n->FirstChildElement("TTL") == nullptr)
	{
		printf("Error TTL for controller %d.\n", _id);
	}
	else
	{
		_ttl = atoi(n->FirstChildElement("TTL")->GetText());
		printf("                   TTL: %d\n", _ttl);
	}

	if (n->FirstChildElement("MinMillisBetweenPackets") == nullptr)
	{
		printf("Error reading Minimum Milliseconds Between Packets setting for controller %d.\n", _id);
	}
	else
	{
		_minmillisbetweenpackets = atol(n->FirstChildElement("MinMillisBetweenPackets")->GetText());
		printf("                   Min millisecs between packets: %ld\n", _minmillisbetweenpackets);
	}

	if (n->FirstChildElement("MaxBurst") == nullptr)
	{
		printf("Error reading Max Burst setting for controller %d.\n", _id);
	}
	else
	{
		_maxburst = atoi(n->FirstChildElement("MaxBurst")->GetText());
		printf("                   Max burst packets: %d\n", _maxburst);
	}

	if (n->FirstChildElement("BurstByteLimit") == nullptr)
	{
		printf("Error reading Burst Byte Limit setting for controller %d.\n", _id);
	}
	else
	{
		_burstbytelimit = atoi(n->FirstChildElement("BurstByteLimit")->GetText());
		printf("                   Burst packet byte limit: %d\n", _burstbytelimit);
	}

	if (n->FirstChildElement("MaximumMillisBetweenSends") == nullptr)
	{
		printf("Error reading Maximum Milliseconds Between Sends setting for controller %d.\n", _id);
	}
	else
	{
		_maxmillisbetweensends = atol(n->FirstChildElement("MaximumMillisBetweenSends")->GetText());
		printf("                   Max millisecs between mandatory sends: %ld\n", _maxmillisbetweensends);
	}

	if (n->FirstChildElement("Brightness") == nullptr)
	{
		printf("Error reading Brightness setting for controller %d.\n", _id);
	}
	else
	{
		_brightness = atol(n->FirstChildElement("Brightness")->GetText()) & 0xFF;
		printf("                   Brightness: %d\n", _brightness);
	}

	if (n->FirstChildElement("WhiteAdjustment") == nullptr)
	{
		printf("Error reading WhiteAdjustment setting for controller %d.\n", _id);
	}
	else
	{
		_whiteadjustment = atol(n->FirstChildElement("WhiteAdjustment")->GetText()) & 0xFF;
		printf("                   WhiteAdjustment: %d\n", _whiteadjustment);
	}

    if (_brightness != 255 && _whiteadjustment != 255)
    {
        printf("                   WhiteAdjustment ignored as brightness setting overrules it.");
    }

	strcpy_s(_szipaddress, n->FirstChildElement("IPAddress")->GetText());

	tinyxml2::XMLElement* universes = n->FirstChildElement("Universes");
	tinyxml2::XMLElement* u = universes->FirstChildElement("Universe");

	while (u != nullptr)
	{
		int uni = atoi(u->GetText());
		_universes.push_back(uni);
		printf("                   Target universe: %d\n", uni);
		Universe::CreateUniverse(this, _universelist, uni, _szipaddress, _port, _ttl, _uniqueportperoutput, &_outevent, &_outevent_mutex, traceuniverse);
		u = u->NextSiblingElement();
	}

	int localport = 0;
#ifdef RASPBERRY
	if (!_uniqueportperoutput)
	{
		_socket = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
		if (_socket == -1)
		{
			printf("Error creating socket %d.\n", errno);
		}
		memset(&_ipAddress, 0x00, sizeof(_ipAddress));
		_ipAddress.sin_family = AF_INET;
		_ipAddress.sin_port = htons(_port);
		if (inet_aton(_szipaddress, &_ipAddress.sin_addr) == 0)
		{
			printf("Error converting ip address %s.\n", _szipaddress);
		}
		if (setsockopt(_socket, IPPROTO_IP, IP_TTL, (const char*)&_ttl, sizeof(_ttl)) != 0)
		{
			printf("Error setting socket TTL %d.\n", errno);
		}

		sockaddr_in local;
		local.sin_family = AF_INET;
		local.sin_addr.s_addr = INADDR_ANY;
		local.sin_port = 0; //randomly selected port
		if (bind(_socket, (sockaddr*)&local, sizeof(local)) != 0)
		{
			printf("Error binding to socket : %d.\n", errno);
		}

		struct sockaddr_in sin;
		socklen_t addrlen = sizeof(sin);
		if (getsockname(_socket, (struct sockaddr *)&sin, &addrlen) == 0 &&
			sin.sin_family == AF_INET &&
			addrlen == sizeof(sin))
		{
			localport = ntohs(sin.sin_port);
		}
		else
		{
			printf("Error getting socket local port : %d.\n", errno);
		}
	}
	pthread_create(&_thread, nullptr, &ControllerRun, (void *)this);
	pthread_create(&_copythread, nullptr, &CopyRun, (void *)this);
#else
	if (!_uniqueportperoutput)
	{
		_socket = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
		if (_socket == INVALID_SOCKET)
		{
			printf("Error creating socket %d.\n", WSAGetLastError());
		}
		_ipAddress.sin_family = AF_INET;
		_ipAddress.sin_port = htons(_port);
		if (inet_pton(AF_INET, _szipaddress, &(_ipAddress.sin_addr)) != 1)
		{
			printf("Error converting ip address %s : %d.\n", _szipaddress, WSAGetLastError());
		}
		int rc = setsockopt(_socket, IPPROTO_IP, IP_TTL, reinterpret_cast<const char*>(&_ttl), sizeof(_ttl));
		if (rc != 0)
		{
			printf("Error setting socket TTL %d.\n", rc);
		}

		sockaddr_in local;
		local.sin_family = AF_INET;
		local.sin_addr.s_addr = INADDR_ANY;
		local.sin_port = 0; //randomly selected port
		if (::bind(_socket, reinterpret_cast<sockaddr*>(&local), sizeof(local)) != 0)
		{
			printf("Error binding to socket : %d.\n", WSAGetLastError());
		}

		struct sockaddr_in sin;
		int addrlen = sizeof(sin);
		if (getsockname(_socket, reinterpret_cast<struct sockaddr *>(&sin), &addrlen) == 0 &&
			sin.sin_family == AF_INET &&
			addrlen == sizeof(sin))
		{
			localport = ntohs(sin.sin_port);
		}
		else
		{
			printf("Error getting socket local port : %d.\n", WSAGetLastError());
		}
	}
	_thread = CreateThread(nullptr, 0, ControllerRun, this, 0, &_threadId);
	_copythread = CreateThread(nullptr, 0, CopyRun, this, 0, &_copythreadId);
#endif
	if (!_uniqueportperoutput)
	{
		printf("                   IP Address: %d.%d.%d.%d Port %d.\n", (_ipAddress.sin_addr.s_addr & 0x000000FF), ((_ipAddress.sin_addr.s_addr & 0x0000FF00) >> 8), ((_ipAddress.sin_addr.s_addr & 0x00FF0000) >> 16), ((_ipAddress.sin_addr.s_addr & 0xFF000000) >> 24), localport);
	}
}

#ifdef RASPBERRY
void* ControllerRun(void* lpParam)
{
	Controller* c = (Controller*)lpParam;
	printf("Controller %d sending thread started.\n", c->Id());
	c->Run();
	printf("Controller %d sending thread exited.\n", c->Id());
    pthread_exit(0);
	return nullptr;
}
void* CopyRun(void* lpParam)
{
	Controller* c = (Controller*)lpParam;
	printf("Controller %d copy thread started.\n", c->Id());
	c->CopyRunFunction();
	printf("Controller %d copy thread exited.\n", c->Id());
	pthread_exit(0);
	return nullptr;
}
#else
DWORD WINAPI ControllerRun( LPVOID lpParam )
{
	Controller* c = static_cast<Controller*>(lpParam);
	printf("Controller %d sending thread started.\n", c->Id());
	c->Run();
	printf("Controller %d sending thread exited.\n", c->Id());
	return 0;
}
DWORD WINAPI CopyRun(LPVOID lpParam)
{
	Controller* c = static_cast<Controller*>(lpParam);
	printf("Controller %d copy thread started.\n", c->Id());
	c->CopyRunFunction();
	printf("Controller %d copy thread exited.\n", c->Id());
	return 0;
}
#endif

